use soutien;

SELECT 
    users.email,
    contreparties.status,
    quoi,
    taille,
    datec 
FROM contreparties 
LEFT JOIN users 
ON contreparties.user_id = users.id
INTO OUTFILE '/tmp/contreparties.csv' 
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT 
    users.email,
    dons.
    status,
    lang,
    somme,
    dons.id,
    datec,
    NULL,
    identifier
FROM dons 
LEFT JOIN users 
ON dons.user_id = users.id
INTO OUTFILE '/tmp/dons.csv'
FIELDS TERMINATED BY ';'
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

SELECT 
    users.email,
    users.total,
    users.cumul,
    users.commentaire,
    users.pseudo,
    users.pseudo,
    adresse,
    adresse2,
    codepostal,
    ville,
    etat,
    pays 
FROM adresses
LEFT JOIN users 
ON adresses.user_id = users.id 
ORDER BY RAND()
INTO OUTFILE '/tmp/adresses_and_users.csv' 
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n';
