<?php

namespace App\EventListener;

use App\Entity\Donation;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::postPersist, method: 'postPersist', entity: Donation::class)]
#[AsEntityListener(event: Events::preUpdate, method: 'preUpdate', entity: Donation::class)]
#[AsEntityListener(event: Events::postUpdate, method: 'postUpdate', entity: Donation::class)]
class DonationListener
{
    private $previousAmount = null;

    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function postPersist(Donation $donation, PostPersistEventArgs $args): void
    {
        $this->updateUserTotalCumul($donation);
    }

    public function preUpdate(Donation $donation, PreUpdateEventArgs $args): void
    {
        if (isset($args->getEntityChangeSet()['amount'])) {
            $this->previousAmount = $args->getEntityChangeSet()['amount'][0];
        }
    }

    public function postUpdate(Donation $donation, PostUpdateEventArgs $args): void
    {
        $this->updateUserTotalCumul($donation);
    }

    public function updateUserTotalCumul(Donation $donation)
    {
        if (($user = $donation->getUser()) && !in_array($donation->getStatus(), [Donation::STATUS_PUNCTUAL_NOT_VALID, Donation::STATUS_MONTHLY_NOT_VALID, Donation::STATUS_IN_PROGRESS])) {
            if (!$donation->isUserTotalCumulUpdated()) {
                // Donation validation
                $user->setTotal($user->getTotal() + $donation->getAmount());
                $user->setCumul($user->getCumul() + $donation->getAmount());
                $donation->setUserTotalCumulUpdated(true);
                $this->entityManager->flush();
            } elseif (null !== $this->previousAmount) {
                // Donation amount changed
                $amountDiff = $donation->getAmount() - $this->previousAmount;
                $user->setTotal($user->getTotal() + $amountDiff);
                $user->setCumul($user->getCumul() + $amountDiff);
                $this->entityManager->flush();
            }
        }
    }
}