<?php

namespace App\Command;

use App\Service\Import\UserCsvImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand('app:import:user')]
class ImportUserCommand extends AbstractImportCommand
{
    public function __construct(
        private UserCsvImportService $userCsvImportService,
        TranslatorInterface          $translator
    )
    {
        parent::__construct($translator);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->error) {
            return Command::FAILURE;
        }
        $ts = fn($message) => $this->translator->trans($message);
        $csvData = $this->userCsvImportService->getCsvData($this->filePath);
        $progressBar = new ProgressBar($output, count($csvData));
        $importStart = time();
        $results = $this->userCsvImportService->processImport($csvData, $this->test, $progressBar);
        $importDuration = time() - $importStart;
        $this->displayImportResults($results, $this->test, $importDuration, $output, $ts);

        return Command::SUCCESS;
    }
}