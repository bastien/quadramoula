<?php

namespace App\Command;

use App\Service\Import\CompensationCsvImportService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[AsCommand('app:import:compensation')]
class ImportCompensationCommand extends AbstractImportCommand
{
    public function __construct(
        private CompensationCsvImportService $compensationCsvImportService,
        TranslatorInterface                  $translator
    )
    {
        parent::__construct($translator);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->error) {
            return Command::FAILURE;
        }
        $ts = fn($message) => $this->translator->trans($message);
        $csvData = $this->compensationCsvImportService->getCsvData($this->filePath);
        $progressBar = new ProgressBar($output, count($csvData));
        $importStart = time();
        $results = $this->compensationCsvImportService->processImport($csvData, $this->test, $progressBar);
        $importDuration = time() - $importStart;
        $this->displayImportResults($results, $this->test, $importDuration, $output, $ts);

        return Command::SUCCESS;
    }
}