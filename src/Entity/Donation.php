<?php

namespace App\Entity;

use App\Repository\DonationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;

#[ORM\Entity(repositoryClass: DonationRepository::class)]
#[ORM\Index(name: "transaction_idx", columns: ["transaction"])]
#[Gedmo\Loggable]
class Donation implements TimestampableInterface
{
    use TimestampableTrait;

    const STATUS_PUNCTUAL_NOT_VALID = 0;
    const STATUS_PUNCTUAL_VALID = 1;
    const STATUS_IN_PROGRESS = 42;   // TODO Keep only if used in old site imported data ?
    const STATUS_MONTHLY_NOT_VALID = 100;
    const STATUS_MONTHLY_VALID = 101;
    const STATUS_MONTHLY_HANDED = 102; // TODO Keep only if used in old site imported data ?
    //const STATUS_MONTHLY_CANCELLED = 103;

    const PAYMENT_METHOD_CB = 1;
    const PAYMENT_METHOD_CHECK = 2;
    const PAYMENT_METHOD_BANKWIRE = 3;
    const PAYMENT_METHOD_CASH = 4;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'donations')]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $user = null;

    #[ORM\Column]
    #[Gedmo\Versioned]
    private ?int $status = null;

    #[ORM\Column(length: 255)]
    private ?string $locale = 'fr';

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $amount = null;

    #[ORM\Column]
    private ?int $paymentMethod = null;

    #[ORM\ManyToOne(inversedBy: 'donations')]
    private ?UserBankAlias $userBankAlias = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $transaction = null;

    #[ORM\Column]
    private ?bool $userTotalCumulUpdated = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusString(): ?string
    {
        switch ($this->getStatus()) {
            case self::STATUS_PUNCTUAL_NOT_VALID:
                return 'donation.status.punctual_not_valid';
            case self::STATUS_PUNCTUAL_VALID:
                return 'donation.status.punctual_valid';
            case self::STATUS_IN_PROGRESS:
                return 'donation.status.in_progress';
            case self::STATUS_MONTHLY_NOT_VALID:
                return 'donation.status.monthly_not_valid';
            case self::STATUS_MONTHLY_VALID:
                return 'donation.status.monthly_valid';
            case self::STATUS_MONTHLY_HANDED:
                return 'donation.status.monthly_handed';
            //case self::STATUS_MONTHLY_CANCELLED:
            //    return 'donation.status.monthly_cancelled';
            default:
                return null;
        }
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): static
    {
        $this->locale = $locale;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): static
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPaymentMethod(): ?int
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(int $paymentMethod): static
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getPaymentMethodString(): ?string
    {
        switch ($this->getPaymentMethod()) {
            case self::PAYMENT_METHOD_CB:
                return 'donation.payment_method.cb';
            case self::PAYMENT_METHOD_CHECK:
                return 'donation.payment_method.check';
            case self::PAYMENT_METHOD_BANKWIRE:
                return 'donation.payment_method.bankwire';
            case self::PAYMENT_METHOD_CASH:
                return 'donation.payment_method.cash';
            default:
                return null;
        }
    }

    public function getExportData()
    {
        return [
            'fields.date' => $this->getCreatedAt(),
            'fields.user' => $this->getUser(),
            'fields.amount' => $this->getAmount(),
            'fields.status' => $this->getStatusString(),
            'fields.payment_method' => $this->getPaymentMethodString(),
        ];
    }

    public function getUserBankAlias(): ?UserBankAlias
    {
        return $this->userBankAlias;
    }

    public function setUserBankAlias(?UserBankAlias $userBankAlias): static
    {
        $this->userBankAlias = $userBankAlias;

        return $this;
    }

    public function getTransaction(): ?string
    {
        return $this->transaction;
    }

    public function setTransaction(?string $transaction): static
    {
        $this->transaction = $transaction;

        return $this;
    }

    public function isUserTotalCumulUpdated(): ?bool
    {
        return $this->userTotalCumulUpdated;
    }

    public function setUserTotalCumulUpdated(bool $userTotalCumulUpdated): static
    {
        $this->userTotalCumulUpdated = $userTotalCumulUpdated;

        return $this;
    }
}
