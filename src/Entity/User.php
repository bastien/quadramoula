<?php

namespace App\Entity;

use App\Repository\UserRepository;
use App\Validator\Constraints as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'email_exists.message')]
#[ORM\Index(name: "email_idx", columns: ["email"])]
#[Gedmo\Loggable]
class User implements UserInterface, PasswordAuthenticatedUserInterface, TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private array $roles = ['ROLE_USER'];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    /**
     * @var string
     */
    #[AppAssert\PasswordStrength(message: 'password_strengh.message')]
    private $plainPassword;

    #[ORM\Column(length: 255)]
    #[Gedmo\Versioned]
    private ?string $email = null;

    #[ORM\Column]
    private ?bool $active = true;

    #[ORM\Column]
    #[Gedmo\Versioned]
    private ?int $total = 0;

    #[ORM\Column]
    #[Gedmo\Versioned]
    private ?int $cumul = 0;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $comment = null;

    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    private ?Address $address = null;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Donation::class)]
    private Collection $donations;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Compensation::class)]
    private Collection $compensations;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserBankAlias::class, cascade: ['remove'])]
    private Collection $userBankAliases;

    public function __construct()
    {
        $this->donations = new ArrayCollection();
        $this->compensations = new ArrayCollection();
        $this->userBankAliases = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->getEmail();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): static
    {
        $this->total = $total;

        return $this;
    }

    public function getCumul(): ?int
    {
        return $this->cumul;
    }

    public function setCumul(int $cumul): static
    {
        $this->cumul = $cumul;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): static
    {
        // set the owning side of the relation if necessary
        if (null !== $address && $address->getUser() !== $this) {
            $address->setUser($this);
        }

        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Donation>
     */
    public function getDonations(): Collection
    {
        return $this->donations;
    }

    public function addDonation(Donation $donation): static
    {
        if (!$this->donations->contains($donation)) {
            $this->donations->add($donation);
            $donation->setUser($this);
        }

        return $this;
    }

    public function removeDonation(Donation $donation): static
    {
        if ($this->donations->removeElement($donation)) {
            // set the owning side to null (unless already changed)
            if ($donation->getUser() === $this) {
                $donation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Compensation>
     */
    public function getCompensations(): Collection
    {
        return $this->compensations;
    }

    public function addCompensation(Compensation $compensation): static
    {
        if (!$this->compensations->contains($compensation)) {
            $this->compensations->add($compensation);
            $compensation->setUser($this);
        }

        return $this;
    }

    public function removeCompensation(Compensation $compensation): static
    {
        if ($this->compensations->removeElement($compensation)) {
            // set the owning side to null (unless already changed)
            if ($compensation->getUser() === $this) {
                $compensation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserBankAlias>
     */
    public function getUserBankAliases(): Collection
    {
        return $this->userBankAliases;
    }

    public function addUserBankAlias(UserBankAlias $userBankAlias): static
    {
        if (!$this->userBankAliases->contains($userBankAlias)) {
            $this->userBankAliases->add($userBankAlias);
            $userBankAlias->setUser($this);
        }

        return $this;
    }

    public function removeUserBankAlias(UserBankAlias $userBankAlias): static
    {
        if ($this->userBankAliases->removeElement($userBankAlias)) {
            // set the owning side to null (unless already changed)
            if ($userBankAlias->getUser() === $this) {
                $userBankAlias->setUser(null);
            }
        }

        return $this;
    }
}
