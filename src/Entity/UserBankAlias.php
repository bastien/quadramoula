<?php

namespace App\Entity;

use App\Repository\UserBankAliasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;

#[ORM\Entity(repositoryClass: UserBankAliasRepository::class)]
#[ORM\Index(name: "bank_alias_idx", columns: ["bank_alias"])]
#[Gedmo\Loggable]
class UserBankAlias implements TimestampableInterface
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'userBankAliases')]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $user = null;

    #[ORM\Column(length: 255)]
    private ?string $bankAlias = null;

    #[ORM\OneToMany(mappedBy: 'userBankAlias', targetEntity: Donation::class)]
    private Collection $donations;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subscription = null;

    public function __construct()
    {
        $this->donations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getBankAlias(): ?string
    {
        return $this->bankAlias;
    }

    public function setBankAlias(string $bankAlias): static
    {
        $this->bankAlias = $bankAlias;

        return $this;
    }

    /**
     * @return Collection<int, Donation>
     */
    public function getDonations(): Collection
    {
        return $this->donations;
    }

    public function addDonation(Donation $donation): static
    {
        if (!$this->donations->contains($donation)) {
            $this->donations->add($donation);
            $donation->setUserBankAlias($this);
        }

        return $this;
    }

    public function removeDonation(Donation $donation): static
    {
        if ($this->donations->removeElement($donation)) {
            // set the owning side to null (unless already changed)
            if ($donation->getUserBankAlias() === $this) {
                $donation->setUserBankAlias(null);
            }
        }

        return $this;
    }

    public function getSubscription(): ?string
    {
        return $this->subscription;
    }

    public function setSubscription(?string $subscription): static
    {
        $this->subscription = $subscription;

        return $this;
    }

}
