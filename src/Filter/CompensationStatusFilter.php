<?php

namespace App\Filter;

use App\Form\Admin\Filter\CompensationStatusFilterType;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;

class CompensationStatusFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setLabel('fields.status')
            ->setFormType(CompensationStatusFilterType::class)
            ->setProperty('status');
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $queryBuilder
            ->andWhere(sprintf('%s.status = :status', $filterDataDto->getEntityAlias()))
            ->setParameter('status', $filterDataDto->getValue());
    }
}