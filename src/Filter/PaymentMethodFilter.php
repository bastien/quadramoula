<?php

namespace App\Filter;

use App\Form\Admin\Filter\PaymentMethodFilterType;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;

class PaymentMethodFilter implements FilterInterface
{
    use FilterTrait;

    public static function new(): self
    {
        return (new self())
            ->setFilterFqcn(__CLASS__)
            ->setLabel('fields.payment_method')
            ->setFormType(PaymentMethodFilterType::class)
            ->setProperty('paymentMethod');
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $queryBuilder
            ->andWhere(sprintf('%s.paymentMethod = :paymentMethod', $filterDataDto->getEntityAlias()))
            ->setParameter('paymentMethod', $filterDataDto->getValue());
    }
}