<?php

namespace App\Controller\Admin\Crud;

use App\Controller\Admin\Crud\Abstract\AbstractExportableCrudController;
use App\Entity\Compensation;
use App\Filter\CompensationAddressFilter;
use App\Filter\CompensationStatusFilter;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CompensationCrudController extends AbstractExportableCrudController
{
    public static function getEntityFqcn(): string
    {
        return Compensation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.compensation.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.compensation.entity.label.plural')
            ->showEntityActionsInlined()
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt', 'fields.date')->hideOnForm(),
            AssociationField::new('user', 'fields.user'),
            TextField::new('address', 'fields.address')->hideOnForm()->renderAsHtml(),
            ChoiceField::new('status', 'fields.status')->setChoices([
                'compensation.status.requested' => Compensation::STATUS_REQUESTED,
                'compensation.status.sent' => Compensation::STATUS_SENT,
                'compensation.status.returned_to_sender' => Compensation::STATUS_RETURNED_TO_SENDER,
                'compensation.status.cancelled' => Compensation::STATUS_CANCELLED,
            ]),
            AssociationField::new('gift', 'fields.gift'),
            AssociationField::new('giftVariation', 'fields.gift_variation'),
            BooleanField::new('keepAddress', 'fields.keep_address'),
            TextareaField::new('comment', 'fields.comment'),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('createdAt')
            ->add('user')
            ->add(CompensationAddressFilter::new())
            ->add(CompensationStatusFilter::new())
            ->add('gift')
            ->add('giftVariation');
    }
}
