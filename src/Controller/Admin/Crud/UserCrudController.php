<?php

namespace App\Controller\Admin\Crud;

use App\Entity\User;
use App\Repository\CompensationRepository;
use App\Repository\DonationRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\Service\Attribute\Required;

class UserCrudController extends AbstractCrudController
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(private DonationRepository $donationRepository, private CompensationRepository $compensationRepository, private AdminUrlGenerator $adminUrlGenerator)
    {
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.user.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.user.entity.label.plural')
            ->showEntityActionsInlined()
            ->setDefaultSort(['id' => 'DESC'])
            ->overrideTemplate('crud/edit', 'admin/crud/user/edit.html.twig')
            ->overrideTemplate('crud/detail', 'admin/crud/user/detail.html.twig');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email', 'fields.email'),
            TextField::new('plainPassword', 'fields.password')->onlyWhenCreating()->setRequired(true),
            TextField::new('plainPassword', 'fields.password')->onlyWhenUpdating()
                ->setHelp('help.password.leave_empty_to_keep'),
            NumberField::new('total', 'fields.total'),
            NumberField::new('cumul', 'fields.cumul'),
            TextareaField::new('comment', 'fields.comment')->hideOnIndex(),
            BooleanField::new('active', 'fields.active'),
            AssociationField::new('address', 'fields.address')->renderAsEmbeddedForm(),
        ];
    }

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);

        $this->addHashPasswordEventListener($formBuilder);

        return $formBuilder;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);

        $this->addHashPasswordEventListener($formBuilder);

        return $formBuilder;
    }

    #[Required]
    public function setHasher(UserPasswordHasherInterface $passwordHasher): void
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('email')
            ->add('address')
            ->add('total')
            ->add('cumul');
    }

    private function addHashPasswordEventListener(FormBuilderInterface $formBuilder)
    {
        $formBuilder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            /** @var User $user */
            $user = $event->getData();
            if ($user->getPlainPassword()) {
                $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));
            }
        });
    }

    private function addDataToResponseParameters($responseParameters)
    {
        if ($responseParameters instanceof KeyValueStore) {
            $user = $responseParameters->get('entity')->getInstance();
            $referrer = $this->adminUrlGenerator
                ->setController(self::class)
                ->setAction(Action::EDIT)
                ->setEntityId($user->getId())
                ->generateUrl();
            $donations = $this->donationRepository->findByUser($user);
            foreach ($donations as &$donation) {
                $donation->actions = [
                    'action.edit' => [
                        'url' => $this->adminUrlGenerator
                            ->setController(DonationCrudController::class)
                            ->setAction(Action::EDIT)
                            ->setEntityId($donation->getId())
                            ->generateUrl(),
                        'classes' => 'action-edit',
                    ],
                    'action.delete' => [
                        'url' => $this->adminUrlGenerator
                            ->setController(DonationCrudController::class)
                            ->setAction(Action::DELETE)
                            ->setEntityId($donation->getId())
                            ->setReferrer($referrer)
                            ->generateUrl(),
                        'classes' => 'action-delete text-danger',
                    ],
                ];
            }
            $responseParameters->set('donations', $donations);
            $compensations = $this->compensationRepository->findByUser($user);
            foreach ($compensations as &$compensation) {
                $compensation->actions = [
                    'action.edit' => [
                        'url' => $this->adminUrlGenerator
                            ->setController(CompensationCrudController::class)
                            ->setAction(Action::EDIT)
                            ->setEntityId($compensation->getId())
                            ->generateUrl(),
                        'classes' => 'action-edit',
                    ],
                    'action.delete' => [
                        'url' => $this->adminUrlGenerator
                            ->setController(CompensationCrudController::class)
                            ->setAction(Action::DELETE)
                            ->setEntityId($compensation->getId())
                            ->setReferrer($referrer)
                            ->generateUrl(),
                        'classes' => 'action-delete text-danger',
                    ],
                ];
            }
            $responseParameters->set('compensations', $compensations);
        }
        return $responseParameters;
    }

    public function edit(AdminContext $context)
    {
        $responseParameters = parent::edit($context);
        return $this->addDataToResponseParameters($responseParameters);
    }

    public function detail(AdminContext $context)
    {
        $responseParameters = parent::detail($context);
        return $this->addDataToResponseParameters($responseParameters);
    }
}
