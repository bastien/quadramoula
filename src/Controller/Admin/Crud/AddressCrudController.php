<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Address;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AddressCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Address::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.address.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.address.entity.label.plural')
            ->showEntityActionsInlined()
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('firstname', 'fields.firstname')->setCssClass('address-field required'),
            TextField::new('lastname', 'fields.lastname')->setCssClass('address-field required'),
            TextField::new('address1', 'fields.address1')->setCssClass('address-field required'),
            TextField::new('address2', 'fields.address2')->setCssClass('address-field'),
            TextField::new('postcode', 'fields.postcode')->setCssClass('address-field required'),
            TextField::new('city', 'fields.city')->setCssClass('address-field required'),
            TextField::new('state', 'fields.state')->setCssClass('address-field'),
            TextField::new('country', 'fields.country')->setCssClass('address-field required'),
        ];
    }
}
