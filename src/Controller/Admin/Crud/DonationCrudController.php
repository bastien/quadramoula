<?php

namespace App\Controller\Admin\Crud;

use App\Controller\Admin\Crud\Abstract\AbstractExportableCrudController;
use App\Entity\Donation;
use App\Filter\DonationStatusFilter;
use App\Filter\PaymentMethodFilter;
use App\Form\Admin\ImportBankDonationFormType;
use App\Service\Import\BankDonationCsvImportService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;

class DonationCrudController extends AbstractExportableCrudController
{
    public static function getEntityFqcn(): string
    {
        return Donation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.donation.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.donation.entity.label.plural')
            ->showEntityActionsInlined()
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt', 'fields.date')->hideOnForm(),
            AssociationField::new('user', 'fields.user')->setRequired(false),
            NumberField::new('amount', 'fields.amount'),
            ChoiceField::new('status', 'fields.status')->setChoices([
                'donation.status.punctual_not_valid' => Donation::STATUS_PUNCTUAL_NOT_VALID,
                'donation.status.punctual_valid' => Donation::STATUS_PUNCTUAL_VALID,
                'donation.status.in_progress' => Donation::STATUS_IN_PROGRESS,
                'donation.status.monthly_not_valid' => Donation::STATUS_MONTHLY_NOT_VALID,
                'donation.status.monthly_valid' => Donation::STATUS_MONTHLY_VALID,
                'donation.status.monthly_handed' => Donation::STATUS_MONTHLY_HANDED,
                //'donation.status.monthly_cancelled' => Donation::STATUS_MONTHLY_CANCELLED,
            ]),
            ChoiceField::new('paymentMethod', 'fields.payment_method')->setChoices([
                'donation.payment_method.cb' => Donation::PAYMENT_METHOD_CB,
                'donation.payment_method.check' => Donation::PAYMENT_METHOD_CHECK,
                'donation.payment_method.bankwire' => Donation::PAYMENT_METHOD_BANKWIRE,
                'donation.payment_method.cash' => Donation::PAYMENT_METHOD_CASH,
            ]),
            TextField::new('transaction', 'fields.transaction')->setDisabled(),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('createdAt')
            ->add('user')
            ->add(DonationStatusFilter::new())
            ->add(PaymentMethodFilter::new())
            ->add('amount')
            ->add('transaction');
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions = parent::configureActions($actions);
        $export = Action::new('import', 'admin.crud.donation.action.import')
            ->setIcon('fa fa-upload')
            ->linkToCrudAction('import')
            ->setCssClass('btn')
            ->createAsGlobalAction();

        return $actions->add(Crud::PAGE_INDEX, $export);
    }

    public function import(Request $request, BankDonationCsvImportService $bankDonationCsvImportService)
    {
        $form = $this->createForm(ImportBankDonationFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $test = $formData['test'];
            $csvData = $bankDonationCsvImportService->getCsvData($form['file']->getData()->getPathname());
            $results = $bankDonationCsvImportService->processImport($csvData, $test);
        }
        return $this->render('admin/crud/donation/import.html.twig', [
            'results' => isset($results) ? $results : null,
            'test' => isset($test) ? $test : null,
            'importForm' => $form->createView(),
        ]);
    }
}
