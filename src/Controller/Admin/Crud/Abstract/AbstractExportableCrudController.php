<?php

namespace App\Controller\Admin\Crud\Abstract;

use App\Service\Export\CsvService;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Factory\FilterFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractExportableCrudController extends AbstractCrudController
{
    private CsvService $csvService;
    private TranslatorInterface $translator;

    public function configureActions(Actions $actions): Actions
    {
        $export = Action::new('export', 'admin.action.export')
            ->setIcon('fa fa-download')
            ->linkToCrudAction('export')
            ->setCssClass('btn')
            ->createAsGlobalAction();

        return $actions->add(Crud::PAGE_INDEX, $export);
    }

    public function export(Request $request, CsvService $csvService, TranslatorInterface $translator)
    {
        $this->csvService = $csvService;
        $this->translator = $translator;
        $context = $request->attributes->get(EA::CONTEXT_REQUEST_ATTRIBUTE);
        $fields = FieldCollection::new($this->configureFields(Crud::PAGE_INDEX));
        $filters = $this->container->get(FilterFactory::class)->create($context->getCrud()->getFiltersConfig(), $fields, $context->getEntity());
        $entities = $this->createIndexQueryBuilder($context->getSearch(), $context->getEntity(), $fields, $filters)
            ->getQuery()
            ->getResult();
        $data = [];
        foreach ($entities as $entity) {
            $data[] = array_map([$this, 'translate'], $entity->getExportData());
        }
        if (count($data) > 0) {
            $data = array_merge([array_map([$this, 'translate'], array_keys($data[0]))], $data);
        }
        return $this->csvService->export($data, $this->translator->trans('admin.export.filename.' . strtolower(str_replace('App\Entity\\', '', $this->getEntityFqcn()))));
    }

    public function translate($value)
    {
        return is_string($value) ? $this->translator->trans($value) : $value;
    }
}
