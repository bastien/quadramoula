<?php

namespace App\Controller\Admin\Crud;

use App\Entity\Cms;
use App\Field\TranslationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CmsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Cms::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.cms.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.cms.entity.label.plural')
            ->showEntityActionsInlined()
            ->setFormThemes([
                '@FOSCKEditor/Form/ckeditor_widget.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig',
                '@A2lixTranslationForm/bootstrap_5_layout.html.twig',
            ]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'fields.title')->hideOnForm(),
            TranslationField::new('translations', 'translations', [
                'title' => [
                    'field_type' => TextType::class,
                    'required' => true,
                    'label' => 'fields.title',
                ],
                'slug' => [
                    'field_type' => TextType::class,
                    'required' => true,
                    'label' => 'fields.slug',
                ],
                'content' => [
                    'field_type' => CKEditorType::class,
                    'required' => true,
                    'label' => 'fields.content',
                ],
            ])->onlyOnForms(),
        ];
    }
}
