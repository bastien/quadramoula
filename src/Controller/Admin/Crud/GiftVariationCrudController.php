<?php

namespace App\Controller\Admin\Crud;

use App\Entity\GiftVariation;
use App\Field\TranslationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GiftVariationCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return GiftVariation::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('admin.crud.gift_variation.entity.label.singular')
            ->setEntityLabelInPlural('admin.crud.gift_variation.entity.label.plural')
            ->showEntityActionsInlined()
            ->setFormThemes([
                '@FOSCKEditor/Form/ckeditor_widget.html.twig',
                '@EasyAdmin/crud/form_theme.html.twig',
                '@A2lixTranslationForm/bootstrap_5_layout.html.twig',
            ]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TranslationField::new('translations', 'translations', [
                'name' => [
                    'field_type' => TextType::class,
                    'required' => true,
                    'label' => 'fields.name',
                ],
            ])->onlyOnForms(),
            AssociationField::new('gift', 'fields.gift'),
            TextField::new('name', 'fields.name')->hideOnForm(),
        ];
    }
}
