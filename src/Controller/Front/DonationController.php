<?php

namespace App\Controller\Front;

use App\Entity\Donation;
use App\Entity\EmailLog;
use App\Entity\UserBankAlias;
use App\Service\MailerService;
use App\Util\PaymentServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DonationController extends AbstractController
{
    public function __construct(
        private PaymentServiceInterface $paymentService,
        private EntityManagerInterface  $entityManager,
        private MailerService           $mailerService,
        private TranslatorInterface     $translator
    )
    {

    }

    #[Route('/{_locale<%app.locales%>}/donation', name: 'donation')]
    public function donation(Request $request)
    {
        $session = $request->getSession();
        if ($request->get('free-amount') || $request->get('amount')) {
            $session->set('amount', (int) ($request->get('free-amount') ?: $request->get('amount')));
            $session->set('monthly', (bool) $request->get('monthly'));
        }
        if (!$session->get('amount')) {
            return $this->redirectToRoute('index');
        }
        return $this->render('front/donation/donation.html.twig', [
            'amount' => $session->get('amount'),
            'monthly' => $session->get('monthly'),
        ]);
    }

    #[Route('/{_locale<%app.locales%>}/donation-process', name: 'donation_process')]
    public function donationProcess(Request $request)
    {
        $amount = (int) ($request->get('free-amount') ?: $request->get('amount'));
        $monthly = (bool) $request->get('monthly');
        if (!$amount) {
            return $this->redirectToRoute('index');
        }
        $donation = new Donation();
        $donation->setUser($this->getUser());
        $donation->setStatus($monthly ? Donation::STATUS_MONTHLY_NOT_VALID : Donation::STATUS_PUNCTUAL_NOT_VALID);
        $donation->setLocale($request->getLocale());
        $donation->setAmount($amount);
        $donation->setPaymentMethod(Donation::PAYMENT_METHOD_CB);
        $this->entityManager->persist($donation);
        $this->entityManager->flush();
        if ($monthly) {
            $bankAlias = substr($donation->getId() . '_' . (null !== $donation->getUser() ? explode('@', $donation->getUser()->getEmail())[0] : 'anonymous'), 0, 50);
            $userBankAlias = new UserBankAlias();
            $userBankAlias->setUser($donation->getUser());
            $userBankAlias->setBankAlias($bankAlias);
            $donation->setUserBankAlias($userBankAlias);
            $this->entityManager->persist($userBankAlias);
            $this->entityManager->flush();
        } else {
            $userBankAlias = null;
        }
        return $this->paymentService->processPaymentRequest($donation, $userBankAlias);
    }

    #[Route('/{_locale<%app.locales%>}/donation-payment-response/{donation}', name: 'donation_payment_response')]
    public function donationPaymentResponse(Request $request, Donation $donation)
    {
        if ($this->paymentService->processPaymentResponse($request, $donation)) {
            $sendEmail = false; // to avoid mutiple emails if multiple calls
            switch ($donation->getStatus()) {
                case Donation::STATUS_PUNCTUAL_NOT_VALID:
                    $donation->setStatus(Donation::STATUS_PUNCTUAL_VALID);
                    $sendEmail = true;
                    break;
                case Donation::STATUS_MONTHLY_NOT_VALID:
                    $donation->setStatus(Donation::STATUS_MONTHLY_VALID);
                    $sendEmail = true;
                    break;
            }
            $this->entityManager->flush();
            if ($sendEmail && null !== $donation->getUser()) {
                $this->mailerService->sendEmail(
                    EmailLog::TYPE_USER,
                    $this->translator->trans('email.donation_confirmation.subject'),
                    [$donation->getUser()->getEmail()],
                    'donation_confirmation',
                    [
                        'type' => $donation->getStatus() == Donation::STATUS_MONTHLY_VALID ? $this->translator->trans('donation.monthly') : $this->translator->trans('donation.punctual'),
                        'amount' => $donation->getAmount(),
                    ]
                );
            }
        }
    }

    #[Route('/{_locale<%app.locales%>}/donation-payment-error/{donation}', name: 'donation_payment_error')]
    public function donationPaymentError(Donation $donation)
    {
        return $this->render('front/donation/payment-error.html.twig', [
            'donation' => $donation,
        ]);
    }

    #[Route('/{_locale<%app.locales%>}/donation-payment-success/{donation}', name: 'donation_payment_success')]
    public function donationPaymentSuccess(Donation $donation)
    {
        return $this->render('front/donation/payment-success.html.twig', [
            'donation' => $donation,
        ]);
    }
}