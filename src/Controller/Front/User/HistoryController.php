<?php

namespace App\Controller\Front\User;

use App\Entity\Donation;
use App\Repository\CompensationRepository;
use App\Repository\DonationRepository;
use App\Util\PaymentServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/{_locale<%app.locales%>}/user/history')]
class HistoryController extends AbstractController
{
    #[Route('', name: 'user_history')]
    public function history(DonationRepository $donationRepository, CompensationRepository $compensationRepository)
    {
        return $this->render('front/user/history.html.twig', [
            'donations' => $donationRepository->findByUser($this->getUser()),
            'compensations' => $compensationRepository->findByUser($this->getUser()),
        ]);
    }

    /*#[Route('/cancel-subscription/{donation}', name: 'user_history_cancel_subscription')]
    public function cancelSubscription(Donation $donation, PaymentServiceInterface $paymentService)
    {
        $paymentService->cancelSubscription($donation);
    }*/
}