<?php

namespace App\Repository;

use App\Entity\UserBankAlias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserBankAlias>
 *
 * @method UserBankAlias|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserBankAlias|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserBankAlias[]    findAll()
 * @method UserBankAlias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserBankAliasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserBankAlias::class);
    }

    public function findOneByBankAlias($alias): ?UserBankAlias
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.bankAlias = :alias')
            ->setParameter('alias', $alias)
            ->getQuery()
            ->getOneOrNullResult();
    }
//    /**
//     * @return UserBankAlias[] Returns an array of UserBankAlias objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UserBankAlias
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
