<?php

namespace App\Repository;

use App\Entity\Compensation;
use App\Entity\Gift;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Compensation>
 *
 * @method Compensation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compensation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compensation[]    findAll()
 * @method Compensation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompensationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compensation::class);
    }

    public function save(Compensation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Compensation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByUser(User $user): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findOneByUserGiftDate(?User $user, ?Gift $gift, DateTime $date): ?Compensation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.user = :user')
            ->andWhere('c.gift = :gift')
            ->andWhere('c.createdAt = :date')
            ->setParameter('user', $user)
            ->setParameter('gift', $gift)
            ->setParameter('date', $date)
            ->getQuery()
            ->getOneOrNullResult();
    }
//    /**
//     * @return Compensation[] Returns an array of Compensation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Compensation
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
