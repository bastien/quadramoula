<?php

namespace App\Service\Payment;

use App\Entity\Donation;
use App\Entity\UserBankAlias;
use App\Repository\DonationRepository;
use App\Util\PaymentServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use DOMXPath;
use Psr\Log\LoggerInterface;
use SoapClient;
use SoapFault;
use SoapHeader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class SystempayPaymentService implements PaymentServiceInterface
{
    public function __construct(private string                 $paymentUrl,
                                private string                 $soapUrlWsdl,
                                private string                 $siteId,
                                private string                 $key,
                                private string                 $ctxMode,
                                private string                 $currency,
                                private UrlGeneratorInterface  $urlGenerator,
                                private TranslatorInterface    $translator,
                                private Environment            $twig,
                                private LoggerInterface        $paymentRequestLogger,
                                private LoggerInterface        $paymentResponseLogger,
                                private DonationRepository     $donationRepository,
                                private EntityManagerInterface $entityManager
    )
    {
    }

    public function processPaymentRequest(Donation $donation, ?UserBankAlias $userBankAlias): Response
    {
        $params = [
            'vads_site_id' => $this->siteId,
            'vads_ctx_mode' => $this->ctxMode,
            'vads_trans_id' => sprintf('%06d', $donation->getId()),
            'vads_trans_date' => (new \DateTime('now', new \DateTimeZone('UTC')))->format('YmdHis'),
            'vads_action_mode' => 'INTERACTIVE',
            'vads_version' => 'V2',
            'vads_validation_mode' => '0',
            'vads_language' => $donation->getLocale(),
            'vads_order_id' => $donation->getId(),
            'vads_url_cancel' => $this->urlGenerator->generate('index', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'vads_url_check' => $this->urlGenerator->generate('donation_payment_response', ['donation' => $donation->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
            'vads_url_error' => $this->urlGenerator->generate('donation_payment_error', ['donation' => $donation->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
            'vads_url_refused' => $this->urlGenerator->generate('index', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'vads_url_success' => $this->urlGenerator->generate('donation_payment_success', ['donation' => $donation->getId()], UrlGeneratorInterface::ABSOLUTE_URL),
            'vads_shop_name' => $this->translator->trans('site_name'),
            'vads_shop_url' => $this->urlGenerator->generate('index', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ];
        switch ($donation->getStatus()) {
            case Donation::STATUS_PUNCTUAL_NOT_VALID:
                $params['vads_page_action'] = 'PAYMENT';
                $params['vads_amount'] = $donation->getAmount() * 100;
                $params['vads_currency'] = $this->currency;
                $params['vads_payment_config'] = 'SINGLE';
                break;
            case Donation::STATUS_MONTHLY_NOT_VALID:
                $params['vads_identifier'] = $userBankAlias->getBankAlias();
                $params['vads_page_action'] = 'REGISTER_SUBSCRIBE';
                $params['vads_sub_effect_date'] = date('Ymd');
                $params['vads_sub_amount'] = $donation->getAmount();
                $params['vads_sub_currency'] = $this->currency;
                $params['vads_sub_desc'] = 'RRULE:FREQ=MONTHLY;BYMONTHDAY=7';
                break;
        }
        $params['vads_cust_email'] = null !== $donation->getUser() ? $donation->getUser()->getEmail() : 'anonymous@laquadrature.net';
        $params['signature'] = $this->calcSignature($params);
        foreach ($params as $key => $val) {
            $this->paymentRequestLogger->log('debug', $key . ' : ' . $val);
        }
        $content = $this->twig->render('front/systempay/payment-form.html.twig', [
            'target' => $this->paymentUrl,
            'params' => $params,
            'donation' => $donation,
        ]);
        return new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }

    public function calcSignature($params)
    {
        ksort($params);
        $signature = '';
        foreach ($params as $key => $value) {
            if (substr($key, 0, 4) == 'vads') {
                $signature .= $value . '+';
            }
        }
        $signature .= $this->key;
        return base64_encode(hash_hmac('sha256', $signature, $this->key, true));
    }

    public function processPaymentResponse(Request $request, Donation $donation): bool
    {
        $params = $request->request->all();
        foreach ($params as $key => $val) {
            $this->paymentResponseLogger->log('debug', $key . ' : ' . $val);
        }
        if ($params['signature'] != $this->calcSignature($params)) {
            $this->paymentResponseLogger->log('error', 'Signature ' . $params['signature'] . ' != ' . $this->calcSignature($params));
            return false;
        }
        if ((int) $params['vads_order_id'] !== $donation->getId()) {
            $this->paymentResponseLogger->log('error', 'Donation ' . $params['vads_order_id'] . ' != ' . $donation->getId());
            return false;
        }
        $donation->setTransaction($params['vads_trans_id']);
        if (isset($params['vads_subscription'])) {
            $userBankAlias = $donation->getUserBankAlias();
            if (null !== $userBankAlias && $userBankAlias->getSubscription() != $params['vads_subscription']) {
                $userBankAlias->setSubscription($params['vads_subscription']);
            }
        }
        $this->entityManager->flush();
        if ($params['vads_result'] != '00') {
            $this->paymentResponseLogger->log('error', 'Payment failed for donation ' . $params['vads_order_id'] . ' result ' . $params['vads_result']);
            return false;
        }
        return true;
    }
/*
    public function cancelSubscription(Donation $donation): bool
    {
        dump($donation);
        $client = new SoapClient($this->soapUrlWsdl, [
            'trace' => 1,
            'exceptions' => 0,
            'encoding' => 'UTF-8',
            'soapaction' => '',
        ]);
        //Generating a header
        $requestId = $this->genSoapUuid();
        $timestamp = gmdate("Y-m-d\TH:i:s\Z");
        $authToken = base64_encode(hash_hmac('sha256', $requestId . $timestamp, $this->key, true));
        $this->setSoapHeaders($this->siteId, $requestId, $timestamp, $this->ctxMode, $authToken, $this->key, $client);
        //Generating a body
        $commonRequest = new commonRequest;
        $queryRequest = new queryRequest;
        $queryRequest->paymentToken = "c975d6af1d5e478da3570d43494d86d2";
        $queryRequest->submissionDate = "2015-08-28T09:21:34+00:00";
        $queryRequest->subscriptionId = "20150828i6VFSA";

        // Call to the cancelSubscription operation
        try {
            $cancelSubscriptionRequest = new cancelSubscription;
            $cancelSubscriptionRequest->commonRequest = $commonRequest;
            $cancelSubscriptionRequest->queryRequest = $queryRequest;
            $cancelSubscriptionResponse = $client->cancelSubscription($cancelSubscriptionRequest);
        } catch (SoapFault $fault) {
            //Managing exceptions
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
        }
        echo "<hr> [Request Header] <br/>", htmlspecialchars($client->__getLastRequestHeaders()), "<br/>";
        echo "<hr> [Request] <br/>", htmlspecialchars($client->__getLastRequest()), "<br/>";
        echo "<hr> [Response Header]<br/>", htmlspecialchars($client->__getLastResponseHeaders()), "<br/>";
        echo "<hr> [Response]<br/>", htmlspecialchars($client->__getLastResponse()), "<br/>";
        echo '<hr>';
        echo "<hr> [Response SOAP Headers]<br/>";
        ///Response analysis
        //Retrieving the SOAP Header of the response to store the headers in a table (here $responseHeader)
        $dom = new DOMDocument;
        $dom->loadXML($client->__getLastResponse(), LIBXML_NOWARNING);
        $path = new DOMXPath($dom);
        $headers = $path->query('//*[local-name()="Header"]/*');
        $responseHeader = [];
        foreach ($headers as $headerItem) {
            $responseHeader[$headerItem->nodeName] = $headerItem->nodeValue;
        }
        //Computation of the authentication token of the response
        $authTokenResponse = base64_encode(hash_hmac('sha256', $responseHeader['timestamp'] .
            $responseHeader['requestId'], $this->key, true));
        if ($authTokenResponse !== $responseHeader['authToken']) {
            //Computation error or attempted fraud echo 'Internal error';
        } else {
            //Response analysis
            if ($cancelSubscriptionResponse->cancelSubscriptionResult->commonResponse->responseCode != "0") {
                //process error
            } else {
                //Process successfully completed
                //Checking the presence of the transactionStatusLabel:
                if (isset ($cancelSubscriptionResponse->cancelSubscriptionResult->commonResponse->transactionStatusLabel)) {
                    //The card is not enrolled or 3DS deactivated
                    // The payment is accepted
                    // The code below must be modified to integrate database updates etc.
                    switch ($cancelSubscriptionResponse->cancelSubscriptionResult->commonResponse->transactionStatusLabel) {
                        case
                        "AUTHORISED":
                            echo "payment accepted";
                            break;
                        case "WAITING_AUTHORISATION":
                            echo "payment accepted";
                            break;
                        case "AUTHORISED_TO_VALIDATE":
                            echo "payment accepted";
                            break;
                        case "WAITING_AUTHORISATION_TO_VALIDATE":
                            echo "payment accepted";
                            break;
                        // The payment is declined
                        default:
                            echo "payment declined";
                            break;
                    }
                }
            }
        }
    }

    private function genSoapUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    private function getSoapAuthToken($requestId, $timestamp, $key)
    {
        $data = $requestId . $timestamp;
        $authToken = hash_hmac("sha256", $data, $key, true);
        $authToken = base64_encode($authToken);
        return $authToken;
    }

    private function setSoapHeaders($shopId, $requestId, $timestamp, $mode, $authToken, $key, $client)
    {
        //shopId, requestId, timestamp, mode and authToken headers creation
        $ns = 'http://v5.ws.vads.lyra.com/Header/';
        $headerShopId = new SOAPHeader ($ns, 'shopId', $shopId);
        $headerRequestId = new SOAPHeader ($ns, 'requestId', $requestId);
        $headerTimestamp = new SOAPHeader ($ns, 'timestamp', $timestamp);
        $headerMode = new SOAPHeader ($ns, 'mode', $mode);
        $authToken = $this->getSoapAuthToken($requestId, $timestamp, $key);
        $headerAuthToken = new SOAPHeader ($ns, 'authToken', $authToken);
        //Adding headers into the SOAP Header
        $headers = [
            $headerShopId,
            $headerRequestId,
            $headerTimestamp,
            $headerMode,
            $headerAuthToken,
        ];
        $client->__setSoapHeaders($headers);
    } */
}

// Webservice definitions
/*class commonRequest
{
    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $comment; // string
}

class commonResponse
{
    public $responseCode; // int
    public $responseCodeDetail; // string
    public $transactionStatusLabel; // string
    public $shopId; // string
    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $paymentToken; // string
}

class cardRequest
{
    public $number; // string
    public $scheme; // string
    public $expiryMonth; // int
    public $expiryYear; // int
    public $cardSecurityCode; // string
    public $cardHolderBirthDay; // dateTime
    public $paymentToken; // string
}

class customerRequest
{
    public $billingDetails; // billingDetailsRequest
    public $shippingDetails; // shippingDetailsRequest
    public $extraDetails; // extraDetailsRequest
}

class billingDetailsRequest
{
    public $reference; // string
    public $title; // string
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $email; // string
    public $streetNumber; // string
    public $address; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $language; // string
    public $cellPhoneNumber; // string
    public $legalName; // string
    public $identityCode; // string
}

class shippingDetailsRequest
{
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $streetNumber; // string
    public $address; // string
    public $address2; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $deliveryCompanyName; // string
    public $shippingSpeed; // deliverySpeed
    public $shippingMethod; // deliveryType
    public $legalName; // string
    public $identityCode; // string
}

class extraDetailsRequest
{
    public $ipAddress; // string
    public $fingerPrintId; // string
}

class shoppintCartRequest
{
    public $insuranceNumber; // long
    public $shippingAmount; // long
    public $taxAmount; // long
    public $cartItemInfo; // cartItemInfo
}

class cartItemInfo
{
    public $productLabel; // string
    public $productType; // productType
    public $productRef; // string
    public $productQty; // int
    public $productAmount; // string
    public $productVat; // string
}

class paymentRequest
{
    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $paymentOptionCode; // string
}

class paymentResponse
{
    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $effectiveAmount; // long
    public $effectiveCurrency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $operationType; // int
    public $creationDate; // dateTime
    public $externalTransactionId; // string
    public $liabilityShift; // string
    public $sequenceNumber; // int
    public $paymentType; // paymentType
    public $paymentError; // int
}

class orderResponse
{
    public $orderId; // string
    public $extInfo; // extInfo
}

class extInfo
{
    public $key; // string
    public $value; // string
}

class cardResponse
{
    public $number; // string
    public $scheme; // string
    public $brand; // string
    public $country; // string
    public $productCode; // string
    public $bankCode; // string
    public $expiryMonth; // int
    public $expiryYear; // int
}

class authorizationResponse
{
    public $mode; // string
    public $amount; // long
    public $currency; // int
    public $date; // dateTime
    public $number; // string
    public $result; // int
}

class captureResponse
{
    public $date; // dateTime
    public $number; // int
    public $reconciliationStatus; // int
    public $refundAmount; // long
    public $refundCurrency; // int
    public $chargeback; // boolean
}

class customerResponse
{
    public $billingDetails; // billingDetailsResponse
    public $shippingDetails; // shippingDetailsResponse
    public $extraDetails; // extraDetailsResponse
}

class billingDetailsResponse
{
    public $reference; // string
    public $title; // string
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $email; // string
    public $streetNumber; // string
    public $address; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $language; // string
    public $cellPhoneNumber; // string
    public $legalName; // string
}

class shippingDetailsResponse
{
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $streetNumber; // string
    public $address; // string
    public $address2; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $deliveryCompanyName; // string
    public $shippingSpeed; // deliverySpeed
    public $shippingMethod; // deliveryType
    public $legalName; // string
    public $identityCode; // string
}

class extraDetailsResponse
{
    public $ipAddress; // string
}

class markResponse
{
    public $amount; // long
    public $currency; // int
    public $date; // dateTime
    public $number; // string
    public $result; // int
}

class threeDSResponse
{
    public $authenticationRequestData; // authenticationRequestData
    public $authenticationResultData; // authenticationResultData
}

class authenticationRequestData
{
    public $threeDSAcctId; // string
    public $threeDSAcsUrl; // string
    public $threeDSBrand; // string
    public $threeDSEncodedPareq; // string
    public $threeDSEnrolled; // string
    public $threeDSRequestId; // string
}

class authenticationResultData
{
    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $cavvAlgorithm; // string
    public $signValid; // string
    public $transactionCondition; // string
}

class extraResponse
{
    public $paymentOptionCode; // string
    public $paymentOptionOccNumber; // int
}

class fraudManagementResponse
{
    public $riskControl; // riskControl
    public $riskAnalysis; // riskAnalysis
    public $riskAssessments; // riskAssessments
}

class shoppingCartResponse
{
    public $cartItemInfo; // cartItemInfo
}

class riskControl
{
    public $name; // string
    public $result; // string
}

class riskAnalysis
{
    public $score; // string
    public $resultCode; // string
    public $status; // vadRiskAnalysisProcessingStatus
    public $requestId; // string
    public $extraInfo; // extInfo
}

class riskAssessments
{
    public $results; // string
}

class techRequest
{
    public $browserUserAgent; // string
    public $browserAccept; // string
}

class orderRequest
{
    public $orderId; // string
    public $extInfo; // extInfo
}

class createPayment
{
    public $commonRequest; // commonRequest
    public $threeDSRequest; // threeDSRequest
    public $paymentRequest; // paymentRequest
    public $orderRequest; // orderRequest
    public $cardRequest; // cardRequest
    public $customerRequest; // customerRequest
    public $techRequest; // techRequest
    public $shoppingCartRequest; // shoppingCartRequest
}

class threeDSRequest
{
    public $mode; // threeDSMode
    public $requestId; // string
    public $pares; // string
    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $algorithm; // string
}

class createPaymentResponse
{
    public $createPaymentResult; // createPaymentResult
}

class createPaymentResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $shoppingCartResponse; // shoppingCartResponse
}

class cancelToken
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
}

class cancelTokenResponse
{
    public $cancelTokenResult; // cancelTokenResult
}

class cancelTokenResult
{
    public $commonResponse; // commonResponse
}

class queryRequest
{
    public $uuid; // string
    public $orderId; // string
    public $subscriptionId; // string
    public $paymentToken; // string
}

class wsResponse
{
    public $requestId; // string
}

class createToken
{
    public $commonRequest; // commonRequest
    public $cardRequest; // cardRequest
    public $customerRequest; // customerRequest
}

class createTokenResponse
{
    public $createTokenResult; // createTokenResult
}

class createTokenResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $shoppingCartResponse; // shoppingCartResponse
}

class subscriptionResponse
{
    public $subscriptionId; // string
    public $effectDate; // dateTime
    public $cancelDate; // dateTime
    public $initialAmount; // long
    public $rrule; // string
    public $description; // string
    public $initialAmountNumber; // int
    public $pastPaymentNumber; // int
    public $totalPaymentNumber; // int
    public $amount; // long
    public $currency; // int
}

class getTokenDetails
{
    public $queryRequest; // queryRequest
}

class getTokenDetailsResponse
{
    public $getTokenDetailsResult; // getTokenDetailsResult
}

class getTokenDetailsResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $subscriptionResponse; // subscriptionResponse
    public $extraResponse; // extraResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $threeDSResponse; // threeDSResponse
    public $tokenResponse; // tokenResponse
}

class updateSubscription
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
    public $subscriptionRequest; // subscriptionRequest
}

class subscriptionRequest
{
    public $subscriptionId; // string
    public $effectDate; // dateTime
    public $amount; // long
    public $currency; // int
    public $initialAmount; // long
    public $initialAmountNumber; // int
    public $rrule; // string
    public $description; // string
}

class updateSubscriptionResponse
{
    public $updateSubscriptionResult; // updateSubscriptionResult
}

class updateSubscriptionResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
}

class capturePayment
{
    public $settlementRequest; // settlementRequest
}

class settlementRequest
{
    public $transactionUuids; // string
    public $commission; // double
    public $date; // dateTime
}

class capturePaymentResponse
{
    public $capturePaymentResult; // capturePaymentResult
}

class capturePaymentResult
{
    public $commonResponse; // commonResponse
}

class findPayments
{
    public $queryRequest; // queryRequest
}

class findPaymentsResponse
{
    public $findPaymentsResult; // findPaymentsResult
}

class findPaymentsResult
{
    public $commonResponse; // commonResponse
    public $orderResponse; // orderResponse
    public $transactionItem; // transactionItem
}

class transactionItem
{
    public $transactionUuid; // string
    public $transactionStatusLabel; // string
    public $amount; // long
    public $currency; // int
    public $expectedCaptureDate; // dateTime
}

class refundPayment
{
    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $queryRequest; // queryRequest
}

class refundPaymentResponse
{
    public $refundPaymentResult; // refundPaymentResult
}

class refundPaymentResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $fraudManagementResponse; // fraudManagementResponse
}

class verifyThreeDSEnrollment
{
    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $cardRequest; // cardRequest
    public $techRequest; // techRequest
}

class verifyThreeDSEnrollmentResponse
{
    public $verifyThreeDSEnrollmentResult; // verifyThreeDSEnrollmentResult
}

class verifyThreeDSEnrollmentResult
{
    public $commonResponse; // commonResponse
    public $threeDSResponse; // threeDSResponse
}

class reactivateToken
{
    public $queryRequest; // queryRequest
}

class reactivateTokenResponse
{
    public $reactivateTokenResult; // reactivateTokenResult
}

class reactivateTokenResult
{
    public $commonResponse; // commonResponse
}

class createSubscription
{
    public $commonRequest; // commonRequest
    public $orderRequest; // orderRequest
    public $subscriptionRequest; // subscriptionRequest
    public $cardRequest; // cardRequest
}

class createSubscriptionResponse
{
    public $createSubscriptionResult; // createSubscriptionResult
}

class createSubscriptionResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $shoppingCartResponse; // shoppingCartResponse
}

class cancelSubscription
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
}

class cancelSubscriptionResponse
{
    public $cancelSubscriptionResult; // cancelSubscriptionResult
}

class cancelSubscriptionResult
{
    public $commonResponse; // commonResponse
}

class updatePayment
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
    public $paymentRequest; // paymentRequest
}

class updatePaymentResponse
{
    public $updatePaymentResult; // updatePaymentResult
}

class updatePaymentResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
}

class validatePayment
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
}

class validatePaymentResponse
{
    public $validatePaymentResult; // validatePaymentResult
}

class validatePaymentResult
{
    public $commonResponse; // commonResponse
}

class cancelPayment
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
}

class cancelPaymentResponse
{
    public $cancelPaymentResult; // cancelPaymentResult
}

class cancelPaymentResult
{
    public $commonResponse; // commonResponse
}

class checkThreeDSAuthentication
{
    public $commonRequest; // commonRequest
    public $threeDSRequest; // threeDSRequest
}

class checkThreeDSAuthenticationResponse
{
    public $checkThreeDSAuthenticationResult; // checkThreeDSAuthenticationResult
}

class checkThreeDSAuthenticationResult
{
    public $commonResponse; // commonResponse
    public $threeDSResponse; // threeDSResponse
}

class getPaymentDetails
{
    public $queryRequest; // queryRequest
}

class getPaymentDetailsResponse
{
    public $getPaymentDetailsResult; // getPaymentDetailsResult
}

class getPaymentDetailsResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $subscriptionResponse; // subscriptionResponse
    public $extraResponse; // extraResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $threeDSResponse; // threeDSResponse
    public $tokenResponse; // tokenResponse
}

class duplicatePayment
{
    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $orderRequest; // orderRequest
    public $queryRequest; // queryRequest
}

class duplicatePaymentResponse
{
    public $duplicatePaymentResult; // duplicatePaymentResult
}

class duplicatePaymentResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $fraudManagementResponse; // fraudManagementResponse
}

class updateToken
{
    public $commonRequest; // commonRequest
    public $queryRequest; // queryRequest
    public $cardRequest; // cardRequest
    public $customerRequest; // customerRequest
}

class updateTokenResponse
{
    public $updateTokenResult; // updateTokenResult
}

class updateTokenResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
}

class getPaymentUuid
{
    public $legacyTransactionKeyRequest; // legacyTransactionKeyRequest
}

class legacyTransactionKeyRequest
{
    public $transactionId; // string
    public $sequenceNumber; // int
    public $creationDate; // dateTime
}

class getPaymentUuidResponse
{
    public $legacyTransactionKeyResult; // legacyTransactionKeyResult
}

class legacyTransactionKeyResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
}

class getSubscriptionDetails
{
    public $queryRequest; // queryRequest
}

class getSubscriptionDetailsResponse
{
    public $getSubscriptionDetailsResult; // getSubscriptionDetailsResult
}

class getSubscriptionDetailsResult
{
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $subscriptionResponse; // subscriptionResponse
    public $extraResponse; // extraResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $threeDSResponse; // threeDSResponse
    public $tokenResponse; // tokenResponse
}

class paymentType
{
    const SINGLE = 'SINGLE';
    const INSTALLMENT = 'INSTALLMENT';
    const SPLIT = 'SPLIT';
    const SUBSCRIPTION = 'SUBSCRIPTION';
    const RETRY = 'RETRY';
}

class custStatus
{
    const _PRIVATE = 'PRIVATE';
    const COMPANY = 'COMPANY';
}

class deliverySpeed
{
    const STANDARD = 'STANDARD';
    const EXPRESS = 'EXPRESS';
}

class deliveryType
{
    const RECLAIM_IN_SHOP = 'RECLAIM_IN_SHOP';
    const RELAY_POINT = 'RELAY_POINT';
    const RECLAIM_IN_STATION = 'RECLAIM_IN_STATION';
    const PACKAGE_DELIVERY_COMPANY = 'PACKAGE_DELIVERY_COMPANY';
    const ETICKET = 'ETICKET';
}

class vadRiskAnalysisProcessingStatus
{
    const P_TO_SEND = 'P_TO_SEND';
    const P_SEND_KO = 'P_SEND_KO';
    const P_PENDING_AT_ANALYZER = 'P_PENDING_AT_ANALYZER';
    const P_SEND_OK = 'P_SEND_OK';
    const P_MANUAL = 'P_MANUAL';
    const P_SKIPPED = 'P_SKIPPED';
    const P_SEND_EXPIRED = 'P_SEND_EXPIRED';
}

class threeDSMode
{
    const DISABLED = 'DISABLED';
    const ENABLED_CREATE = 'ENABLED_CREATE';
    const ENABLED_FINALIZE = 'ENABLED_FINALIZE';
    const MERCHANT_3DS = 'MERCHANT_3DS';
}

class productType
{
    const FOOD_AND_GROCERY = 'FOOD_AND_GROCERY';
    const AUTOMOTIVE = 'AUTOMOTIVE';
    const ENTERTAINMENT = 'ENTERTAINMENT';
    const HOME_AND_GARDEN = 'HOME_AND_GARDEN';
    const HOME_APPLIANCE = 'HOME_APPLIANCE';
    const AUCTION_AND_GROUP_BUYING = 'AUCTION_AND_GROUP_BUYING';
    const FLOWERS_AND_GIFTS = 'FLOWERS_AND_GIFTS';
    const COMPUTER_AND_SOFTWARE = 'COMPUTER_AND_SOFTWARE';
    const HEALTH_AND_BEAUTY = 'HEALTH_AND_BEAUTY';
    const SERVICE_FOR_INDIVIDUAL = 'SERVICE_FOR_INDIVIDUAL';
    const SERVICE_FOR_BUSINESS = 'SERVICE_FOR_BUSINESS';
    const SPORTS = 'SPORTS';
    const CLOTHING_AND_ACCESSORIES = 'CLOTHING_AND_ACCESSORIES';
    const TRAVEL = 'TRAVEL';
    const HOME_AUDIO_PHOTO_VIDEO = 'HOME_AUDIO_PHOTO_VIDEO';
    const TELEPHONY = 'TELEPHONY';
}*/