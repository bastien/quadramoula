<?php

namespace App\Service\Export;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer;
use Symfony\Component\HttpFoundation\StreamedResponse;


class CsvService
{
    public function export(array $data, string $filename): StreamedResponse
    {
        // Create sheet
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->fromArray($data, null, "A1");

        // Download
        $writer = new Writer\Csv($spreadsheet);
        $response = new StreamedResponse(
            function () use ($writer) {
                $writer->save('php://output');
            }
        );
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $filename . '.csv"');
        $response->headers->set('Cache-Control', 'max-age=0');
        return $response;
    }
}