<?php

namespace App\Service\Import;

use App\Entity\Address;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class UserCsvImportService extends AbstractImportService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserRepository         $userRepository,
    )
    {
    }

    public function processImport(array $csvData, bool $test, ?ProgressBar $progressBar = null): array
    {
        set_time_limit(0);
        $results = [];
        foreach ($csvData as $i => $userData) {
            $email = $userData['email'];
            $total = (int) $userData['total'];
            $cumul = (int) $userData['cumul'];
            $comment = $userData['comment'];
            $firstname = $userData['firstname'];
            $lastname = $userData['lastname'];
            $address1 = $userData['address1'];
            $address2 = $userData['address2'];
            $postcode = $userData['postcode'];
            $city = $userData['city'];
            $state = $userData['state'];
            $country = $userData['country'];
            $user = $this->userRepository->findOneByEmail($email);
            if (null !== $user) {
                $results[$i] = 'exists';
            } else {
                $results[$i] = 'create';
                //if (!$test) {
                $user = new User();
                $user->setEmail($email);
                $user->setPassword('');
                $user->setTotal($total);
                $user->setCumul($cumul);
                $user->setComment($comment);
                if (!$test) {
                    $this->entityManager->persist($user);
                }
                if ($firstname != '' || $lastname != '' || $address1 != '' || $address2 != '' || $postcode != '' || $city != '' || $state != '' || $country != '') {
                    $address = new Address();
                    $address->setUser($user);
                    $address->setFirstname($firstname);
                    $address->setLastname($lastname);
                    $address->setAddress1($address1);
                    $address->setAddress2($address2);
                    $address->setPostcode($postcode);
                    $address->setCity($city);
                    $address->setState($state);
                    $address->setCountry($country);
                    if (!$test) {
                        $this->entityManager->persist($address);
                    }
                }
                //}
            }
            if (!$test) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            if (null !== $progressBar) {
                $progressBar->advance();
            }
        }
        return $results;
    }
}