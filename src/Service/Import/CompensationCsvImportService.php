<?php

namespace App\Service\Import;

use App\Entity\Compensation;
use App\Entity\User;
use App\Repository\CompensationRepository;
use App\Repository\GiftRepository;
use App\Repository\GiftVariationRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class CompensationCsvImportService extends AbstractImportService
{
    public function __construct(
        private EntityManagerInterface  $entityManager,
        private UserRepository          $userRepository,
        private CompensationRepository  $compensationRepository,
        private GiftRepository          $giftRepository,
        private GiftVariationRepository $giftVariationRepository,
    )
    {
    }

    public function processImport(array $csvData, bool $test, ?ProgressBar $progressBar = null): array
    {
        set_time_limit(0);
        $results = [];
        foreach ($csvData as $i => $compensationData) {
            $email = $compensationData['email'];
            $status = (int) $compensationData['status'];
            $gift = $this->giftRepository->findOneByRefImport($compensationData['gift']);
            $giftVariation = $this->giftVariationRepository->findOneByGiftAndRefImport($gift, $compensationData['gift_variation']);
            $date = new DateTime($compensationData['date']);
            $user = $this->userRepository->findOneByEmail($email);
            if (null === $gift) {
                $results[$i] = 'gift_not_found';
            } else {
                $compensation = $this->compensationRepository->findOneByUserGiftDate($user, $gift, $date);
                if (null !== $compensation) {
                    $results[$i] = 'exists';
                } else {
                    $results[$i] = 'create';
                    //if (!$test) {
                    if (null === $user) {
                        $user = new User();
                        $user->setEmail($email);
                        $user->setPassword('');
                        if (!$test) {
                            $this->entityManager->persist($user);
                        }
                    }
                    $compensation = new Compensation();
                    $compensation->setUser($user);
                    $compensation->setStatus($status);
                    $compensation->setGift($gift);
                    $compensation->setGiftVariation($giftVariation);
                    $compensation->setCreatedAt($date);
                    if (!$test) {
                        $this->entityManager->persist($compensation);
                    }
                    //}
                }
            }
            if (!$test) {
                $this->entityManager->flush();
                $this->entityManager->clear();
            }
            if (null !== $progressBar) {
                $progressBar->advance();
            }
        }
        return $results;
    }
}