<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportDonationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', FileType::class, [
                'label' => 'admin.import.file',
            ])
            ->add('test', CheckboxType::class, [
                'label' => 'admin.import.test',
                'required' => false,
                'data' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'admin.import.submit',
            ]);
    }
}