# Quadramoula


## Groupe Matrix pour le développement 

`#quadramoula:laquadrature.net`

## Name
Donations website for La Quadrature Du Net

## Description
Symfony 6 web application


## Requirements
LAMP webserver, with :
- PHP 8.1+
- MySQL / MariaDB

## Installation
PHP dependencies install :
```
composer install
```
Assets build :
```
yarn install
yarn build
```
Assets install :
```
php bin/console ckeditor:install --tag=4.20.1
php bin/console assets:install
```
Setup your .env.local file : (example)
```
###> symfony/framework-bundle ###
APP_ENV=prod
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
DATABASE_URL="mysql://dbuser:dbpassword@dbserver:3306/dbname?serverVersion=8.0.32&charset=utf8mb4"
###< doctrine/doctrine-bundle ###

###> symfony/mailer ###
FROM_EMAIL=email@domain.com
MAILER_DSN=smtp://smtpuser:smtppassword@smtpserver:587
###< symfony/mailer ###

###> symfony/security ###
SECURE_SCHEME=https
###< symfony/security ###

###> Systempay ###  
SYSTEMPAY_PAYMENT_URL=https://paiement.systempay.fr/vads-payment/
SYSTEMPAY_SOAP_URL_WSDL=https://paiement.systempay.fr/vads-ws/v5?wsdl
SYSTEMPAY_SITE_ID=00000000  
SYSTEMPAY_KEY=0000000000000000  
SYSTEMPAY_CTX_MODE=PRODUCTION  	# TEST or PRODUCTION
SYSTEMPAY_CURRENCY=978		# 978 for Euro
###< Systempay ###
```
See :
* https://symfony.com/doc/current/configuration.html#selecting-the-active-environment
* https://symfony.com/doc/current/the-fast-track/en/8-doctrine.html#changing-the-default-database-url-value-in-env
* https://symfony.com/doc/current/mailer.html#transport-setup
* https://paiement.systempay.fr/doc/fr-FR/documentation.html

Make database tables :
```
php bin/console doctrine:migrations:migrate
```
Load database fixtures for prod (default data) :
```
APP_ENV=prod php bin/console hautelook:fixtures:load
```
## Commands
Create your first admin user (required to log in to /admin for the first time) :
```
php bin/console app:make:adminuser
php bin/console app:make:adminuser --username=admin --email=admin@example.org --firstname=Joe --lastname=Doe --password=password --no-interaction
```

Purge old email logs (should be scheduled in a cron task)
```
php bin/console app:email:log:cleanup
```
### Data Import commands
You can check the specifications about import file formats in admin data import section.
Data import is supposed to be done in admin if the volume of data is small enough, and in command line if the data volume is higher.

Users import :
```
php bin/console app:import:user
php bin/console app:import:user --file-path=users.csv --test=1 --no-interaction 
```
Donations import :
```
php bin/console app:import:donation
php bin/console app:import:donation --file-path=donations.csv --test=1 --no-interaction 
```
Compensations import :
```
php bin/console app:import:compensation
php bin/console app:import:compensation --file-path=compensations.csv --test=1 --no-interaction 
```
## Developpment
Build assets on the fly :
```
yarn watch
```
Load database fixtures for dev (fake data) :
```
APP_ENV=dev php bin/console hautelook:fixtures:load
```
This creates fake data, and admin / user access :

Admin : 
* admin / password

Users :
 * alice@example.org / password
 * bob@example.org / password

## Admin
Administration is accessible on /admin URI

## Roadmap & Cahier des charges 

This project was developped in accordance to the [Cahier des charges](cahier-des-charges.md).

## License
TODO
For open source projects, say how it is licensed.

