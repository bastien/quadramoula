import $ from 'jquery';

export default {

    isAddressEmpty: null,

    init: function () {
        let t = this;
        t.initCompensationForm();
    },

    initCompensationForm: function () {
        let t = this;
        $('#compensation-form input[name="gift"]').change(function () {
            t.validateCompensationForm();
            $('#compensation-form label').removeClass('active');
            $(this).closest('label').addClass('active');
        });
        t.validateCompensationForm();
        $('#compensation-form select').click(function () {
            $(this).closest('label').find('input[type="radio"]').prop('checked', true);
        });
    },

    validateCompensationForm: function () {
        let compensationForm = $('#compensation-form');
        compensationForm.find('button[type="submit"]').prop('disabled', compensationForm.hasClass('no-user-address') || compensationForm.find('input[name="gift"]:checked').length === 0);
    }
}
