import $ from 'jquery';
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle';

export default {

    modal: null,

    init: function () {
        let t = this;
        t.modal = new bootstrap.Modal('#modal');
        t.initModalAjaxLinks();
        t.initModalAjaxForms();
    },

    initModalAjaxLinks: function () {
        let t = this;
        $('a[data-ajax-modal]').click(function () {
            $.get($(this).attr('href') + '?ajax=1')
                .done(function (data) {
                    $('#modal .modal-body').html($(data).filter('#layout').find('#content').html());
                    t.modal.show();
                    t.initModalAjaxLinks();
                    t.initModalAjaxForms();
                });
            return false;
        });
    },

    initModalAjaxForms: function () {
        let t = this;
        $('#modal form:not([data-target-main]), form[data-target-modal]:not(.modal-initialized)').submit(function (e) {
            e.preventDefault();
            if ($(this)[0].checkValidity()) {
                $.post(
                    $(this).attr('action') + '?ajax=1',
                    $(this).serialize(),
                    function (data) {
                        let redirectUrl = $(data).filter('link[rel="canonical"]').attr('href');
                        if (typeof redirectUrl !== 'undefined') {
                            document.location.href = redirectUrl;
                        }
                        else {
                            $('#modal .modal-body').html($(data).filter('#layout').find('#content').html());
                            t.initModalAjaxLinks();
                            t.initModalAjaxForms();
                            t.modal.show();
                        }
                    });
            }
            return false;
        }).addClass('modal-initialized');
    },
}
