import $ from 'jquery';

export default {

    init: function () {
        let t = this;
        t.initDonationForm();
    },

    initDonationForm: function () {
        let t = this;
        let form = $('#donation-form');
        form.find('input[name="amount"]').change(function () {
            form.find('input[name="free-amount"]').val('');
            t.validateDonationForm();
        });
        form.find('input[name="free-amount"]').on('keyup change', function () {
            let freeAmount = $(this).val();
            if (freeAmount !== '' && parseFloat(freeAmount) > 0) {
                form.find('input[name="amount"]').prop('checked', false);
            }
            t.validateDonationForm();
        });
        t.validateDonationForm();
    },

    validateDonationForm: function () {
        let form = $('#donation-form');
        let freeAmount = form.find('input[name="free-amount"]').val();
        form.find('button[type=submit]').prop('disabled', (freeAmount === '' || parseFloat(freeAmount) <= 0) && form.find('input[name="amount"]:checked').length === 0)
    },
}
