import $ from 'jquery';

export default {

    isAddressEmpty: null,

    init: function () {
        let t = this;
        t.initAddressForm();
    },

    initAddressForm: function () {
        let t = this;
        let body = $('body');
        if (body.hasClass('ea-edit-User') || body.hasClass('ea-new-User')) {
            $('.address-field input').keyup(t.checkAddressRequired).keyup();
        }
    },

    /**
     * If the user starts typing an address, we make address fields required (as an address will be saved).
     * If every address field is empty, we make the address fields not required (in this case no address will be saved)
     */
    checkAddressRequired: function () {
        let t = this;
        let isAddressEmpty = true;
        $('.address-field input').each(function () {
            if ($(this).val() !== '') {
                isAddressEmpty = false;
            }
        });
        if (isAddressEmpty !== t.isAddressEmpty) {
            if (isAddressEmpty) {
                $('.address-field.required input').prop('required', false);
                $('.address-field.required label').removeClass('required');
            } else {
                $('.address-field.required input').prop('required', true);
                $('.address-field.required label').addClass('required');
            }
            t.isAddressEmpty = isAddressEmpty;
        }
    }
}
