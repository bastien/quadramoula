<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230830211503 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, address1 VARCHAR(255) NOT NULL, address2 VARCHAR(255) DEFAULT NULL, postcode VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, state VARCHAR(255) DEFAULT NULL, country VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_D4E6F81A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE admin_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_AD8A54A9F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE compensation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, gift_id INT NOT NULL, gift_variation_id INT DEFAULT NULL, status INT NOT NULL, comment LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_B2DD12DAA76ED395 (user_id), INDEX IDX_B2DD12DA97A95A83 (gift_id), INDEX IDX_B2DD12DAE9086F1D (gift_variation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE donation (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, user_bank_alias_id INT DEFAULT NULL, status INT NOT NULL, locale VARCHAR(255) NOT NULL, amount NUMERIC(10, 2) NOT NULL, payment_method INT NOT NULL, transaction VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_31E581A0A76ED395 (user_id), INDEX IDX_31E581A08198FD35 (user_bank_alias_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email_log (id INT AUTO_INCREMENT NOT NULL, sender VARCHAR(255) NOT NULL, recipient VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, sent_at DATETIME NOT NULL, type INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE faq (id INT AUTO_INCREMENT NOT NULL, anchor VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faq_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, question VARCHAR(255) NOT NULL, response LONGTEXT NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_50A668562C2AC5D3 (translatable_id), UNIQUE INDEX faq_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift (id INT AUTO_INCREMENT NOT NULL, donation_amount_needed INT NOT NULL, image VARCHAR(255) DEFAULT NULL, image2 VARCHAR(255) DEFAULT NULL, ref_import VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_831C8F582C2AC5D3 (translatable_id), UNIQUE INDEX gift_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_variation (id INT AUTO_INCREMENT NOT NULL, gift_id INT NOT NULL, ref_import VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_F4D49E8097A95A83 (gift_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gift_variation_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, locale VARCHAR(5) NOT NULL, INDEX IDX_9C92FF862C2AC5D3 (translatable_id), UNIQUE INDEX gift_variation_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, total INT NOT NULL, cumul INT NOT NULL, comment LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_bank_alias (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, bank_alias VARCHAR(255) NOT NULL, subscription VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_4CA1630DA76ED395 (user_id), INDEX bank_alias_idx (bank_alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE compensation ADD CONSTRAINT FK_B2DD12DAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE compensation ADD CONSTRAINT FK_B2DD12DA97A95A83 FOREIGN KEY (gift_id) REFERENCES gift (id)');
        $this->addSql('ALTER TABLE compensation ADD CONSTRAINT FK_B2DD12DAE9086F1D FOREIGN KEY (gift_variation_id) REFERENCES gift_variation (id)');
        $this->addSql('ALTER TABLE donation ADD CONSTRAINT FK_31E581A0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE donation ADD CONSTRAINT FK_31E581A08198FD35 FOREIGN KEY (user_bank_alias_id) REFERENCES user_bank_alias (id)');
        $this->addSql('ALTER TABLE faq_translation ADD CONSTRAINT FK_50A668562C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES faq (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_translation ADD CONSTRAINT FK_831C8F582C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES gift (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gift_variation ADD CONSTRAINT FK_F4D49E8097A95A83 FOREIGN KEY (gift_id) REFERENCES gift (id)');
        $this->addSql('ALTER TABLE gift_variation_translation ADD CONSTRAINT FK_9C92FF862C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES gift_variation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_bank_alias ADD CONSTRAINT FK_4CA1630DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81A76ED395');
        $this->addSql('ALTER TABLE compensation DROP FOREIGN KEY FK_B2DD12DAA76ED395');
        $this->addSql('ALTER TABLE compensation DROP FOREIGN KEY FK_B2DD12DA97A95A83');
        $this->addSql('ALTER TABLE compensation DROP FOREIGN KEY FK_B2DD12DAE9086F1D');
        $this->addSql('ALTER TABLE donation DROP FOREIGN KEY FK_31E581A0A76ED395');
        $this->addSql('ALTER TABLE donation DROP FOREIGN KEY FK_31E581A08198FD35');
        $this->addSql('ALTER TABLE faq_translation DROP FOREIGN KEY FK_50A668562C2AC5D3');
        $this->addSql('ALTER TABLE gift_translation DROP FOREIGN KEY FK_831C8F582C2AC5D3');
        $this->addSql('ALTER TABLE gift_variation DROP FOREIGN KEY FK_F4D49E8097A95A83');
        $this->addSql('ALTER TABLE gift_variation_translation DROP FOREIGN KEY FK_9C92FF862C2AC5D3');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE user_bank_alias DROP FOREIGN KEY FK_4CA1630DA76ED395');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE admin_user');
        $this->addSql('DROP TABLE compensation');
        $this->addSql('DROP TABLE donation');
        $this->addSql('DROP TABLE email_log');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE faq');
        $this->addSql('DROP TABLE faq_translation');
        $this->addSql('DROP TABLE gift');
        $this->addSql('DROP TABLE gift_translation');
        $this->addSql('DROP TABLE gift_variation');
        $this->addSql('DROP TABLE gift_variation_translation');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_bank_alias');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
